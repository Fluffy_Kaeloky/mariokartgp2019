﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DisplayManager : MonoBehaviour {

    [SerializeField]
    private RectTransform mainMenuPanel;
    [SerializeField]
    private RectTransform raceSettingsPanel;
    [SerializeField]
    private RectTransform highScoresPanel;
    private EventSystem eventSystem = null;

    private void Start()
    {
        this.eventSystem = EventSystem.current;
    }


    public void OnNewGameClick()
    {
        if (raceSettingsPanel == null)
        {
            Debug.LogError("Couldn't find Race Settings Panel");
            return;
        }

        this.mainMenuPanel.gameObject.SetActive(false);
        this.highScoresPanel.gameObject.SetActive(false);
        this.raceSettingsPanel.gameObject.SetActive(true);

        eventSystem.SetSelectedGameObject(raceSettingsPanel.GetComponent<RaceSettingsPanel>().GetDefaultSelectedObject());
    }

    public void OnMainMenuClick()
    {
        if (mainMenuPanel == null)
        {
            Debug.LogError("Couldn't find Main Menu Panel");
            return;
        }

        this.raceSettingsPanel.gameObject.SetActive(false);
        this.highScoresPanel.gameObject.SetActive(false);
        this.mainMenuPanel.gameObject.SetActive(true);

        eventSystem.SetSelectedGameObject(mainMenuPanel.GetComponent<MainMenuUI>().GetDefaultSelectedObject());
    }

    public void OnHighScoreClick()
    {
        if (highScoresPanel == null)
        {
            Debug.LogError("Couldn't find High Scores Panel");
            return;
        }

        this.mainMenuPanel.gameObject.SetActive(false);
        this.raceSettingsPanel.gameObject.SetActive(false);
        this.highScoresPanel.gameObject.SetActive(true);

        eventSystem.SetSelectedGameObject(highScoresPanel.GetComponent<HighScoresMenu>().GetDefaultSelectedObject());
    }
    
}
