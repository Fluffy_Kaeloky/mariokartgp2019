﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HighScoreMaker : MonoBehaviour {

    [SerializeField]
    private InputField nicknameInputField;
    [SerializeField]
    private RaceTimer raceTimer;
    [SerializeField]
    private Text scoreDisplayer;

    private List<HighScore> highscoreList;

	// Use this for initialization
	void Start () {
        this.highscoreList = new List<HighScore>();
	}
	
    public void OnCreateScoreClick()
    {
        Debug.Log("Bonsoir monsieur ?");

        HighScore newHighScore = new HighScore(this.nicknameInputField.GetComponent<InputField>().text, this.raceTimer.GetTimerStruct());
        this.highscoreList.Add(newHighScore);

        RefreshDisplayer();
    }

    private void RefreshDisplayer()
    {
        this.scoreDisplayer.text = "";

        Debug.Log(this.highscoreList.Count);

        for (uint i = 0; i < this.highscoreList.Count; ++i)
        {
            this.scoreDisplayer.text += this.highscoreList[(int)i].name  + " - "+ this.highscoreList[(int)i].GetScoreAsString() + "\n";
        }
    }
        

    public void OnSaveScoresClick()
    {
        HighScoreSerializer serializer = new HighScoreSerializer("HighScoresTest.xml");
        serializer.SerializeObject(this.highscoreList);
    }

    public void OnLoadScoresClick()
    {
        HighScoreSerializer serializer = new HighScoreSerializer("HighScoresTest.xml");
        this.highscoreList.Clear();
        this.highscoreList = serializer.DeserializeHighScore();

        RefreshDisplayer();
    }
}
