﻿using UnityEngine;
using System.Collections;

public abstract class InputBase : MonoBehaviour
{
    public abstract float GetDir();
    public abstract float GetAcceleration();

    public OnFireEvent onFire;
}

[System.Serializable]
public class OnFireEvent : UnityEngine.Events.UnityEvent<OnFireEventArgs> { }

public enum E_THROW_DIR
{
    FRONT,
    REAR,
    NONE
}

[System.Serializable]
public class OnFireEventArgs
{
    public E_THROW_DIR dir;

    public OnFireEventArgs(E_THROW_DIR d)
    {
        dir = d;
    }
}
