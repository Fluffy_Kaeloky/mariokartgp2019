﻿using UnityEngine;
using System.Collections;
using System;

public class IAInput : InputBase
{
    private float dir = 0.0f;
    private float acceleration = 0.0f;

    public override float GetAcceleration()
    {
        return acceleration;
    }

    public override float GetDir()
    {
        return dir;
    }

    public void SetAcceleration(float acc)
    {
        acceleration = acc;
    }

    public void SetDir(float d)
    {
        dir = d;
    }
}
