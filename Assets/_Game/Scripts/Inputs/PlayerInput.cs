﻿using UnityEngine;


public class PlayerInput : InputBase
{
    public override float GetAcceleration()
    {
        float acc = 0.0f;
        acc = Input.GetAxis("Vertical");

        if (acc == 0.0f)
            acc = Input.GetAxis("Right Trigger") - Input.GetAxis("Left Trigger");

        return acc;
    }

    public override float GetDir()
    {
        float dir = 0.0f;
        dir = Input.GetAxis("Horizontal");
        if (dir == 0.0f)
            dir = Input.GetAxis("Left Stick X Axis");

        return dir;
    }

    void Update()
    {
        if ((Input.GetAxis("Vertical") < 0 || Input.GetAxis("Left Stick Y Axis") < 0 )&& Input.GetButton("Fire"))
        {
            if (onFire != null)
                onFire.Invoke(new OnFireEventArgs(E_THROW_DIR.REAR));
        }

        if (Input.GetButton("Fire") && !Input.GetKey(KeyCode.DownArrow))
        {
            if (onFire != null)
                onFire.Invoke(new OnFireEventArgs(E_THROW_DIR.FRONT));
        }

        if (Input.GetButton("Fire") && Input.GetKey(KeyCode.DownArrow))
        {
            if (onFire != null)
                onFire.Invoke(new OnFireEventArgs(E_THROW_DIR.REAR));
        }
    }
}
