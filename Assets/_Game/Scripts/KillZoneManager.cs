﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillZoneManager : MonoBehaviour
{
    public InGameUI uiSystem = null;
    public float killY = -50.0f;

    private List<ProgressTracker> respawningPlayers = new List<ProgressTracker>();
    private ProgressTracker[] trackers = new ProgressTracker[0];
    
    private void Start()
    {
        List<ProgressTracker> list = new List<ProgressTracker>(GameManager.Instance.IACars);
        list.Add(GameManager.Instance.PlayerCar);
        trackers = list.ToArray();
    }

    private void Update()
    {
        foreach (var t in trackers)
        {
            if (t.transform.position.y < killY && respawningPlayers.Find(x => x == t) == null)
                StartCoroutine(RespawnPlayer(t));
        }
    }

    private IEnumerator RespawnPlayer(ProgressTracker t)
    {
        respawningPlayers.Add(t);
        t.GetComponent<Rigidbody>().isKinematic = true;
        t.GetComponent<HoverCarControl>().enabled = false;

        if (t.gameObject.tag == "Player")
        {
            if (uiSystem != null)
                uiSystem.FadeScreen(1.0f, 1.0f);
        }

        yield return new WaitForSeconds(1.0f);
        float offset = 10.0f;

        Vector3 startPos = t.transform.position = t.GetLastValidatedCheckpoint().transform.position + Vector3.up * offset;
        t.transform.rotation = t.GetLastValidatedCheckpoint().transform.rotation;

        float timer = 2.0f;
        float maxTimer = timer;
        while (timer > 0.0f)
        {
            t.transform.position = Vector3.Lerp(startPos, startPos - Vector3.up * offset, (maxTimer - timer) / maxTimer);

            timer -= Time.fixedDeltaTime;

            yield return new WaitForFixedUpdate();
        }

        t.GetComponent<Rigidbody>().isKinematic = false;
        t.GetComponent<HoverCarControl>().enabled = true;

        respawningPlayers.Remove(t);
    }
}
