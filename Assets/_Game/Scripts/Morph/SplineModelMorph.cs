﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class SplineModelMorph : MonoBehaviour
{
    public BezierSpline spline = null;
    public Vector3 scanAxis = Vector3.forward;

    /// <summary>
    /// DO NOT set it in runtime.
    /// </summary>
    public uint meshRepeatCount = 1;

    private MeshFilter filter = null;

    private GameObject[] gameObjects = null;

    private Vector3[] baseVertices = new Vector3[0];
    private Bounds baseBounds;

    //private float scanAxisExtremumBegin;
    //private float scanAxisExtremumEnd;

    private void Start()
    {
        scanAxis.Normalize();

        filter = GetComponent<MeshFilter>();

        baseVertices = filter.mesh.vertices;
        baseBounds = filter.mesh.bounds;

        gameObjects = new GameObject[meshRepeatCount];
        for (int i = 0; i < gameObjects.Length; ++i)
        {
            gameObjects[i] = new GameObject();
            gameObjects[i].name = "Clone : " + i;
            gameObjects[i].transform.SetParent(gameObject.transform, false);
            MeshFilter f = gameObjects[i].AddComponent<MeshFilter>();
            f.mesh = filter.sharedMesh;
            gameObjects[i].AddComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
        }
    }

    private void Update()
    {
        Vector3[] vertices = baseVertices.Clone() as Vector3[];

        for (int i = 0; i < meshRepeatCount; i++)
        {
            //Find extremums
            Vector3 scaledBound = baseBounds.extents;
            scaledBound.Scale(scanAxis);
            float scanAxisBoundMag = scaledBound.magnitude;

            Vector3 scaledBoundCenter = baseBounds.center;
            scaledBoundCenter.Scale(scanAxis);
            float scanAxisBoundCenterMag = scaledBoundCenter.magnitude;

            float scanAxisExtremumBegin = scanAxisBoundCenterMag - scanAxisBoundMag;
            float scanAxisExtremumEnd = scanAxisBoundCenterMag + scanAxisBoundMag;

            //Get the gameObject
            GameObject go = gameObjects[i];

            MeshFilter f = go.GetComponent<MeshFilter>();
            Mesh mesh = f.mesh;

            float progressDelta = 1.0f / meshRepeatCount;

            //Transform mesh
            for (int j = 0; j < vertices.Length; j++)
            {
                Vector3 scaledVertice = baseVertices[j];
                scaledVertice.Scale(scanAxis);
                float scanAxisVerticeMag = scaledVertice.magnitude;
                float normalizedScanPos = (scanAxisVerticeMag - scanAxisExtremumBegin) / (scanAxisExtremumEnd - scanAxisExtremumBegin);

                Vector3 splinePos = spline.GetPoint((normalizedScanPos / meshRepeatCount) + progressDelta * i) - transform.position;
                Vector3 defaultCenter = Vector3.Lerp(scanAxis * scanAxisExtremumBegin, scanAxis * scanAxisExtremumEnd, normalizedScanPos);

                Vector3 diff = splinePos - defaultCenter;

                vertices[j] = baseVertices[j] + diff;
            }

            //Apply
            mesh.vertices = vertices;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
        }
    }

    /*private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            if (filter != null)
            {
                Mesh mesh = filter.mesh;

                Bounds b = mesh.bounds;

                Gizmos.color = Color.blue;
                Gizmos.DrawWireCube(transform.position + b.center, b.size);
                Gizmos.DrawWireSphere(transform.position + b.center, 1.0f);
            }

            Gizmos.color = Color.green;
            Gizmos.DrawCube(transform.position + scanAxis * scanAxisExtremumBegin, Vector3.one);

            Gizmos.color = Color.red;
            Gizmos.DrawCube(transform.position + scanAxis * scanAxisExtremumEnd, Vector3.one);
        }
    }*/
}
