﻿using UnityEngine;
using System.Collections;

public class EnableOnGameStateChange : MonoBehaviour
{
    public bool autoRegister = true;

    public Index[] indexes = new Index[0];

    private void Awake()
    {
        if (autoRegister)
            GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChanged);
    }

    public void OnGameStateChanged(OnGameStateChangedArgs args)
    {
        foreach (var i in indexes)
        {
            if (args.newState == i.state)
            {
                if (i.obj is MonoBehaviour)
                {
                    MonoBehaviour b = i.obj as MonoBehaviour;
                    b.enabled = i.value;
                }
                else if (i.obj is Collider)
                {
                    Collider c = i.obj as Collider;
                    c.enabled = i.value;
                }
                else if (i.obj is GameObject)
                {
                    GameObject go = i.obj as GameObject;
                    go.SetActive(i.value);
                }
                else
                    Debug.LogError("The object given isn't a MonoBehaviour, a Collider nor a GameObject.");
            }
        }
    }

    [System.Serializable]
    public class Index
    {
        public Object obj = null;
        public bool value = true;
        public E_GAMESTATE state = E_GAMESTATE.START;
    }
}
