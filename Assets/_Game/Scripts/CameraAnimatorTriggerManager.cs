﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class CameraAnimatorTriggerManager : MonoBehaviour
{
    public string mushroomTriggerName = "FOVInOut";
    public string speedBoostTriggerName = "FOVInOut";

    private Animator animator = null;


    private void Start()
    {
        animator = GetComponent<Animator>();

        UsableObjectLauncher launcher = GameManager.Instance.PlayerCar.GetComponent<UsableObjectLauncher>();
        if (launcher != null)
        {
            launcher.onObjectFired.AddListener(OnObjectUsed);
            launcher.onGroundedObjectFired.AddListener(OnGroundedObjectUsed);
        }
    }

    public void OnObjectUsed(OnUsableObjectFiredArgs args)
    {
        if (args.objectUsed is MushroomData)
            animator.SetTrigger(mushroomTriggerName);
    }

    public void OnGroundedObjectUsed(OnGroundedObjectFiredArgs args)
    {
        if (args.objectUsed is SpeedBoostZone)
            animator.SetTrigger(speedBoostTriggerName);
    }
}
