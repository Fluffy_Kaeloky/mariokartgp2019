﻿using UnityEngine;
using System.Collections;

public class PersonalEffects : MonoBehaviour
{
    [SerializeField]
    private GameObject greenShellPrefab = null;
    [SerializeField]
    private bool isInvicible = false;
    [SerializeField]
    private bool canHurtEnemies = false;

    public bool IsInvicible { get { return isInvicible; } set { isInvicible = value; } }
    public bool CanHurtEnemies { get { return canHurtEnemies; } set { canHurtEnemies = value; } } 


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Vehicle") && canHurtEnemies)
        {
            if (collision.gameObject.GetComponent<PersonalEffects>().IsInvicible == false)
            {
                GameObject greenShell = Instantiate(greenShellPrefab, collision.gameObject.transform.position, Quaternion.identity) as GameObject;
                greenShell.GetComponent<GreenShell>().ApplyEffect(collision.gameObject);
            }
        }
    }
}
