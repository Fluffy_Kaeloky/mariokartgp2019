﻿using UnityEngine;

public class Helpers : MonoBehaviour
{
    [SerializeField]
    private Transform frontObjSpawnerTrans = null;
    [SerializeField]
    private Transform backObjSpawnerTrans = null;
    [SerializeField]
    private Transform forwardObjDirTrans = null;

    public Transform FrontObjSpawnerTrans { get { return frontObjSpawnerTrans; } }
    public Transform BackObjSpawnerTrans { get { return backObjSpawnerTrans; } }
    public Transform ForwardObjDirTrans { get { return forwardObjDirTrans; } }
}
