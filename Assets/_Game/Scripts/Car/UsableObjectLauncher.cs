﻿using UnityEngine;


public class UsableObjectLauncher : MonoBehaviour {

    [SerializeField]
    private BaseUsableObject usableObject;
    [SerializeField]
    private Transform frontSpawnerPosition;
    [SerializeField]
    private Transform rearSpawnerPosition;
    [SerializeField]
    private Transform forwardDirection;
    [SerializeField]
    private float attackSpeed = 10f;
    [SerializeField]
    private bool isAI = false;
    private bool isActualObjectTripple = false;
    private TripeShellRotator shellRotator = null;
    private float shootTimer = 0f;
    private bool shootTimerDone = false;

    public BaseUsableObject UsableObject    { get { return usableObject; } set { usableObject = value; } }
    public bool IsActualObjectTripple       { get { return isActiveAndEnabled; } }
    public TripeShellRotator ShellRotator { get { return shellRotator; } }

    public OnUsableObjectFired onObjectFired;
    public OnGroundedObjectFired onGroundedObjectFired;


    private void Start()
    {
        shellRotator = GetComponentInChildren<TripeShellRotator>();
    }

    private void Update()
    {
        ManageShootTimer();

        if (isAI && shootTimerDone)
            LaunchInFront();
    }

    private void ManageShootTimer()
    {
        if (shootTimerDone)
            return;

        shootTimer += Time.deltaTime;

        if (shootTimer >= attackSpeed)
        {
            shootTimer = 0f;
            shootTimerDone = true;
        }
    }

    private bool CanLaunch()
    {
        if (usableObject == null)
            return false;

        if (shootTimerDone == false)
            return false;

        return true;
    }

    public void LaunchInFront()
    {
        if (!CanLaunch())
            return;
        else
            shootTimerDone = false;

        if (usableObject.IsNotThrowable)
        {
            UseObject();
            return;
        }

        if (onObjectFired != null)
            onObjectFired.Invoke(new OnUsableObjectFiredArgs(usableObject, E_THROW_DIR.FRONT));

        if (usableObject.IsTripple && isActualObjectTripple == false)
        {
            SpawnTrippleObject();
            return;
        }

        Debug.Log("Launch in Front");
        Vector3 carVelocity = GetComponent<Rigidbody>().velocity;
        GameObject obj = Instantiate(usableObject.Prefab, transform.position, Quaternion.identity) as GameObject;
        (usableObject as BaseFrontRearUsableObject).Use(obj, frontSpawnerPosition, forwardDirection, carVelocity);
        ManageUsableObject();
    }

    public void LaunchInRear()
    {
        if (!CanLaunch())
            return;
        else
            shootTimerDone = false;

        if (usableObject.IsNotThrowable)
        {
            UseObject();
            return;
        }

        if (onObjectFired != null)
            onObjectFired.Invoke(new OnUsableObjectFiredArgs(usableObject, E_THROW_DIR.REAR));

        if (usableObject.IsTripple && isActualObjectTripple == false)
        {
           SpawnTrippleObject();
            return;
        }

        Debug.Log("Launch in Rear");
        GameObject obj = Instantiate(usableObject.Prefab, transform.position, Quaternion.identity) as GameObject;
        (usableObject as BaseFrontRearUsableObject).SecondaryUse(obj, rearSpawnerPosition);
        ManageUsableObject();
    }

    private void UseObject()
    {
        if (onObjectFired != null)
            onObjectFired.Invoke(new OnUsableObjectFiredArgs(usableObject, E_THROW_DIR.NONE));

        Debug.Log("Use not grounded object.");
        GameObject obj = Instantiate(usableObject.Prefab, transform.position, Quaternion.identity) as GameObject;
        obj.GetComponent<BaseNotGroundedObject>().DoAction(gameObject);
        ManageUsableObject();
    }

    private void SpawnTrippleObject()
    {
        Debug.Log("Tripple objects spawned.");
        (usableObject as BaseFrontRearUsableObject).UseTrippleObject(gameObject);

        isActualObjectTripple = true;
    }

    private void ManageUsableObject()
    {
        if (isActualObjectTripple && shellRotator != null)
        {
            shellRotator.RemoveShell();
            if (shellRotator.HasShell() == false)
            {
                isActualObjectTripple = false;
                usableObject = null;
            }
        }
        else
            usableObject = null;
    }

    public void DoGroundedObjectEvent(BaseGroundedObject obj)
    {
        if (onGroundedObjectFired != null)
            onGroundedObjectFired.Invoke(new OnGroundedObjectFiredArgs(obj));
    }
}


[System.Serializable]
public class OnUsableObjectFired : UnityEngine.Events.UnityEvent<OnUsableObjectFiredArgs> { }

[System.Serializable]
public class OnUsableObjectFiredArgs
{
    public BaseUsableObject objectUsed = null;

    public E_THROW_DIR direction = E_THROW_DIR.REAR;

    public OnUsableObjectFiredArgs(BaseUsableObject o, E_THROW_DIR d)
    {
        objectUsed = o;
        direction = d;
    }
}
