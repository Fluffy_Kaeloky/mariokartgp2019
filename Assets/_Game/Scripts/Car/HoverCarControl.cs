﻿using UnityEngine;
using System.Collections;

public class HoverCarControl : MonoBehaviour
{
    public bool hoverEnabled = true;

    Rigidbody body;
    float deadZone = 0.1f;
    public float groundedDrag = 3f;
    public float maxVelocity = 50;
    public float hoverForce = 1000;
    public float gravityForce = 1000f;
    public float hoverHeight = 1.5f;
    public GameObject[] hoverPoints;

    public float forwardAcceleration = 8000f;
    public float reverseAcceleration = 4000f;
    float thrust = 0f;

    public float turnStrength = 1000f;
    float turnValue = 0f;

    public Transform centerOfMass = null;

    float acceleration = 0.0f;
    float steering = 0.0f;

    int layerMask;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        body.centerOfMass = Vector3.down;

        layerMask = 1 << LayerMask.NameToLayer("Vehicle");
        layerMask = ~layerMask;

        if (centerOfMass != null)
            body.centerOfMass = centerOfMass.localPosition;
    }

    //Uncomment this to see a visual indication of the raycast hit points in the editor window
    void OnDrawGizmos()
    {
        RaycastHit hit;
        for (int i = 0; i < hoverPoints.Length; i++)
        {
            if (!hoverPoints[i].activeSelf)
                continue;

            var hoverPoint = hoverPoints[i];
            if (Physics.Raycast(hoverPoint.transform.position,
                                -Vector3.up, out hit,
                                hoverHeight,
                                layerMask))
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(hoverPoint.transform.position, hit.point);
                Gizmos.DrawSphere(hit.point, 0.5f);
            }
            else
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(hoverPoint.transform.position,
                               hoverPoint.transform.position - Vector3.up * hoverHeight);
            }
        }
    }

    void Update()
    {
        // Get thrust input
        thrust = 0.0f;
        if (acceleration > deadZone)
            thrust = acceleration * forwardAcceleration;
        else if (acceleration < -deadZone)
            thrust = acceleration * reverseAcceleration;

        // Get turning input
        turnValue = 0.0f;
        if (Mathf.Abs(steering) > deadZone)
            turnValue = steering;
    }

    void FixedUpdate()
    {
        //  Do hover/bounce force
        RaycastHit hit;
        bool grounded = false;
        for (int i = 0; i < hoverPoints.Length; i++)
        {
            if (hoverEnabled)
            {
                var hoverPoint = hoverPoints[i];
                if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight, layerMask) && hoverPoint.activeSelf)
                {
                    body.AddForceAtPosition(Vector3.up * hoverForce * (1.0f - (hit.distance / hoverHeight)), hoverPoint.transform.position, ForceMode.Force);
                    grounded = true;
                }
            }

           body.AddForceAtPosition(Vector3.down * (gravityForce / hoverPoints.Length), centerOfMass.position, ForceMode.Force);
        }

        if (grounded)
            body.drag = groundedDrag;
        else
        {
            body.drag = 0.1f;
            thrust /= 100f;
            turnValue /= 100f;
        }

        // Handle Forward and Reverse forces
        if (Mathf.Abs(thrust) > 0)
            body.AddForce(transform.forward * thrust);

        if (turnValue > 0)
            body.AddRelativeTorque(Vector3.up * turnValue * turnStrength);
        else if (turnValue < 0)
            body.AddRelativeTorque(Vector3.up * turnValue * turnStrength);

        // Limit max velocity
        if (body.velocity.sqrMagnitude > (body.velocity.normalized * maxVelocity).sqrMagnitude)
        {
            body.velocity = body.velocity.normalized * maxVelocity;
        }
    }

    public void Move(float steering, float acceleration)
    {
        this.acceleration = acceleration;
        this.steering = steering;
    }
}
