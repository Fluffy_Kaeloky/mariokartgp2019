﻿using UnityEngine;
using System.Collections;

public class CarLink : MonoBehaviour
{
    public InputBase inputSystem = null;
    public HoverCarControl carController = null;
    public UsableObjectLauncher usableObjLauncher = null;

    private void Start()
    {
        inputSystem.onFire.AddListener(OnFire);
    }

    private void Update()
    {
        float acc, dir;
        acc = inputSystem.GetAcceleration();
        dir = inputSystem.GetDir();
        carController.Move(dir, acc);
    }

    public void OnFire(OnFireEventArgs args)
    {
        if (!enabled)
            return;

        if (args.dir == E_THROW_DIR.FRONT)
            usableObjLauncher.LaunchInFront();
        else if (args.dir == E_THROW_DIR.REAR)
            usableObjLauncher.LaunchInRear();
    }
}
