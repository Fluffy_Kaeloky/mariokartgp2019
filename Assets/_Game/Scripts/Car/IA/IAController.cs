﻿using UnityEngine;
using System.Collections;

public class IAController : MonoBehaviour
{
    public bool showDebug = true;

    public float dotMultiplier = 30.0f;

    public bool getAnticipationFromGameManager = true;
    public float progressAnticipation = 0.05f;

    public IAInput inputSystem = null;
    public ProgressTracker tracker = null;

    private void Start()
    {
        if (getAnticipationFromGameManager)
            progressAnticipation = GameManager.Instance.TrackSettings.iaAnticipation;
    }

    private void LateUpdate()
    {
        Vector3 groundRelativeForward = Vector3.ProjectOnPlane(transform.forward, Vector3.up);

        Vector3 trackPoint = tracker.Track.GetPoint(tracker.Progress + progressAnticipation);

        Vector3 idealDir = ((trackPoint - transform.position).normalized).normalized;
        idealDir = Vector3.ProjectOnPlane(idealDir, Vector3.up);

        float dir = Mathf.Clamp((1.0f - Vector3.Dot(idealDir, groundRelativeForward)) * dotMultiplier, -1.0f, 1.0f);

        //Check if we should turn right or left
        float sign = Vector3.Dot(idealDir, transform.right);
        dir *= Mathf.Sign(sign);

        if (showDebug)
        {
            Debug.DrawLine(transform.position + Vector3.up * 0.5f, transform.position + groundRelativeForward * 30.0f + Vector3.up * 0.5f, Color.blue);
            Debug.DrawLine(transform.position + Vector3.up * 0.5f, transform.position + idealDir * 30.0f + Vector3.up * 0.5f, Color.red);
            Debug.DrawLine(transform.position + Vector3.up * 0.5f, transform.position + (trackPoint - transform.position), Color.green);
        }

        inputSystem.SetDir(dir);
        inputSystem.SetAcceleration(1.0f);
    }
}
