﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProgressTracker : MonoBehaviour
{
    [SerializeField]
    private bool showDebug = true;

    [SerializeField]
    private BezierSpline track;
    public BezierSpline Track
    {
        get { return track; }
        set
        {
            track = value;
            if (track != null)
                ComputeTrackPoints();
            else
                points = new Vector3[0];
        }
    }

    public bool getProgressPrecisionFromGameManager = true;
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float progress = 0.0f;
    public float Progress { get { return progress; } }

    public float TotalProgress { get { return progress + laps - lapDelta; } }

    [SerializeField]
    private int laps = 0;
    public int Laps { get { return laps; } }

    [SerializeField]
    private float precision = 0.01f;
    public float Precision
    {
        get { return precision; }
        set
        {
            precision = value;
            if (track != null)
                ComputeTrackPoints();
        }
    }

    public int checkpointStartValidateOffset = 1;

    public OnLapValidated onLapValidated;

    public bool getNoTpZoneFromGameManager = true;
    public NoTPZone[] noTPZones = new NoTPZone[0];

    private Vector3[] points = new Vector3[0];
    private Checkpoint[] checkpoints;

    private Checkpoint nextCheckpointToValidate = null;
    private List<Checkpoint> validatedCheckpoints = new List<Checkpoint>();
    private bool lapValidated = false;

    private int lapDelta = 0;
    private float oldProgress = 0.0f;

    private void Start()
    {
        if (getProgressPrecisionFromGameManager)
            precision = GameManager.Instance.TrackSettings.progressTrackersPrecision;

        if (track != null)
            ComputeTrackPoints();

        checkpoints = GameManager.Instance.TrackSettings.Checkpoints;
        foreach (var c in checkpoints)
            c.onCheckpointPass.AddListener(OnCheckpointPassed);
        validatedCheckpoints = new List<Checkpoint>();
        nextCheckpointToValidate = checkpoints[checkpointStartValidateOffset];

        if (getNoTpZoneFromGameManager)
            noTPZones = GameManager.Instance.TrackSettings.noTpZones;

        if (GameManager.Instance.TrackSettings.spawnBeforeStartLine)
            lapDelta = 1;

        RescanProgress();
        oldProgress = progress;
    }

    private void FixedUpdate()
    {
        oldProgress = progress;
        RescanProgress();

        float delta = precision / 1.0f;
        if (oldProgress - progress > delta * 10.0f)
        {
            if (lapDelta != 0)
                lapDelta--;
            else if (lapValidated)
            {
                laps++;
                lapValidated = false;

                if (onLapValidated != null)
                    onLapValidated.Invoke(new OnLapValidatedArgs(this));
            }
        }
        else if (progress - oldProgress > delta * 10.0f)
            lapDelta++;
    }

    private void RescanProgress()
    {
        float delta = precision / 1.0f;
        int closestPointIndex = 0;
        for (int i = 0; i < points.Length; i++)
        {
            foreach (var z in noTPZones)
            {
                float currentProgress = delta * i;

                if (progress >= z.startTPProtectionResult && progress <= z.endTPProtectionResult)
                {
                    if (currentProgress >= z.start && currentProgress <= z.end)
                    {
                        i = Mathf.RoundToInt(((z.end + delta) / delta));
                        break;
                    }
                }
            }

            if (Vector3.Distance(transform.position, points[i]) < Vector3.Distance(transform.position, points[closestPointIndex]))
                closestPointIndex = i;
        }

        progress = delta * closestPointIndex;

        if (showDebug)
            Debug.DrawLine(transform.position, points[closestPointIndex], Color.white);
    }

    private void ComputeTrackPoints()
    {
        float delta = precision / 1.0f;
        points = new Vector3[Mathf.FloorToInt(1.0f / precision)];

        for (int i = 0; i < Mathf.FloorToInt(1.0f / precision); i++)
            points[i] = track.GetPoint(i * delta);

        Debug.Log("ProgressTracker : Processed track, registered " + points.Length + " points.");
    }

    public void OnCheckpointPassed(OnCheckpointPassArgs args)
    {
        if (args.tracker != this)
            return;

        if (nextCheckpointToValidate == args.sender)
        {
            validatedCheckpoints.Add(args.sender);

            if (validatedCheckpoints.Count >= checkpoints.Length)
            {
                lapValidated = true;
                validatedCheckpoints.Clear();
                nextCheckpointToValidate = checkpoints[0];
                return;
            }

            nextCheckpointToValidate = checkpoints[validatedCheckpoints.Count];
        }
    }

    public Checkpoint GetLastValidatedCheckpoint()
    {
        return validatedCheckpoints.Count > 0 ? validatedCheckpoints[validatedCheckpoints.Count - 1] : checkpoints[0];
    }

    private void OnDrawGizmos()
    {
        if (showDebug)
        {
            if (Application.isPlaying)
            {
                foreach (var c in checkpoints)
                {
                    Gizmos.color = Color.green;

                    if (c == nextCheckpointToValidate)
                        Gizmos.color = Color.blue;

                    Gizmos.DrawSphere(c.transform.position, 5.0f);
                }
            }
        }
    }

    [System.Serializable]
    public class NoTPZone
    {
        [Range(0.0f, 1.0f)]
        public float start = 0.0f;

        [Range(0.0f, 1.0f)]
        public float end = 1.0f;

        [Range(0.0f, 1.0f)]
        public float startTPProtectionResult = 0.0f;

        [Range(0.0f, 1.0f)]
        public float endTPProtectionResult = 1.0f;
    }
}

[System.Serializable]
public class OnLapValidated : UnityEngine.Events.UnityEvent<OnLapValidatedArgs> { }

[System.Serializable]
public class OnLapValidatedArgs
{
    public ProgressTracker tracker;

    public OnLapValidatedArgs(ProgressTracker t)
    {
        tracker = t;
    }
}
