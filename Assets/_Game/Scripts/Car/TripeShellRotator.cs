﻿using UnityEngine;
using System.Collections.Generic;

public class TripeShellRotator : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 6f;
    [SerializeField]
    private Transform firstPosition = null;
    [SerializeField]
    private Transform secondPosition = null;
    [SerializeField]
    private Transform thirdPosition = null;
    private List<GameObject> satelliteList = new List<GameObject>();


    private void Update()
    {
        transform.Rotate(Vector3.up, rotationSpeed);
    }

    private void AddSatellites(GameObject prefab, Transform trans)
    {
        GameObject satellite = Instantiate(prefab, trans.position, Quaternion.identity) as GameObject;
        GreenShell shell = satellite.GetComponent<GreenShell>();

        if (shell != null)
        {
            shell.IsChildOfVehicle = true;
            shell.Owner = transform.root.gameObject;
        }

        satellite.GetComponent<Rigidbody>().isKinematic = true;
        satellite.transform.root.parent = trans;

        satelliteList.Add(satellite);
    }

    public void AddShells(GameObject shellPrefab)
    {
        AddSatellites(shellPrefab, firstPosition);
        AddSatellites(shellPrefab, secondPosition);
        AddSatellites(shellPrefab, thirdPosition);
    }

    public void RemoveShell()
    {
        RemoveNextSatellite();
    }

    private void RemoveNextSatellite()
    {
        if (satelliteList.Count > 0)
        {
            Destroy(satelliteList[satelliteList.Count - 1]);
            satelliteList.RemoveAt(satelliteList.Count - 1);
            Debug.Log("TrippleShellRotator : Removed a satellite");
        }
    }

    public bool HasShell()
    {
        if (satelliteList.Count >= 1)
            return true;

        return false;
    }
}
