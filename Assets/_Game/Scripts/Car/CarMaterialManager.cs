﻿using UnityEngine;
using System.Collections;

public class CarMaterialManager : MonoBehaviour
{
    public Renderer targetRenderer = null;
    public CarMaterial[] carMaterials = new CarMaterial[0];

    public void ApplyMaterial(E_CAR_MATERIAL matColor)
    {
        CarMaterial cm = System.Array.Find(carMaterials, x => x.enumMat == matColor);
        if (cm != null && cm.material != null && targetRenderer != null)
            targetRenderer.material = cm.material;
    }

    public enum E_CAR_MATERIAL : int
    {
        BLUE = 0,
        GREEN_AND_MEAN,
        ORANGE,
        PURPLE,
        RED
    }

    public class E_CAR_MATERIALS
    {
        public static readonly int count = 5;
    }

    [System.Serializable]
    public class CarMaterial
    {
        public E_CAR_MATERIAL enumMat = E_CAR_MATERIAL.BLUE;
        public Material material = null;
    }
}
