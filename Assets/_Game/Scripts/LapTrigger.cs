﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LapTrigger : MonoBehaviour
{
    public OnFinishLinePassUnsafe onFinishLinePassUnsafe;

    private void OnTriggerEnter(Collider collider)
    {
        ProgressTracker tracker = null;
        if ((tracker = collider.GetComponentInParent<ProgressTracker>()) != null)
            onFinishLinePassUnsafe.Invoke(new OnFinishLinePassArgs(tracker));
    }
}

[System.Serializable]
public class OnFinishLinePassUnsafe : UnityEvent<OnFinishLinePassArgs> { }

[System.Serializable]
public class OnFinishLinePassArgs
{
    public ProgressTracker tracker = null;

    public OnFinishLinePassArgs(ProgressTracker tracker)
    {
        this.tracker = tracker;
    }
}
