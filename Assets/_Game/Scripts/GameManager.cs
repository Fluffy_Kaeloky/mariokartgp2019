﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    [SerializeField]
    private E_GAMESTATE gameState = E_GAMESTATE.START;
    public E_GAMESTATE GameState
    {
        get { return gameState; }

        set
        {
            if (onGameStateChanged != null)
                onGameStateChanged.Invoke(new OnGameStateChangedArgs(value));

            gameState = value;
        }
    }

    public OnGameStateChanged onGameStateChanged;

    [SerializeField]
    private TrackSettings trackSettings = new TrackSettings();
    public TrackSettings TrackSettings { get { return trackSettings; } }
    private static List<BaseUsableObject> itemList;
    public static List<BaseUsableObject> ItemList { get { return itemList; } }

    public int lapsToWin = 3;
    private ProgressTracker[] iaCars = new ProgressTracker[0];
    public ProgressTracker[] IACars { get { return iaCars; } }

    private ProgressTracker playerCar = null;
    public ProgressTracker PlayerCar { get { return playerCar; } }

    public OnRacerFinishedRace onRacerFinishedRace;

    public bool showTrackDebugCursor = false;
    [Range(0.0f, 1.0f)]
    public float trackDebugCursorPosition = 0.0f;

    private List<ProgressTracker> rankings = new List<ProgressTracker>();
    public List<ProgressTracker> Rankings { get { return rankings; } }

    private HighScoreManager highScoresManager;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.LogError("Multiples instances of class 'GameManager' is present.");
            return;
        }

        instance = this;
        itemList = TrackSettings.ItemsLibrary;

        //Generating Checkpoints using the track's spline
        if (trackSettings.track != null && trackSettings.CheckpointPrefab != null)
        {
            List<Checkpoint> checkpoints = new List<Checkpoint>();

            float progress = 0.0f;
            int i = 0;
            while (progress < 1.0f)
            {
                Checkpoint ch = Instantiate(trackSettings.CheckpointPrefab, trackSettings.track.GetPoint(progress),
                    Quaternion.LookRotation(trackSettings.track.GetDirection(progress), Vector3.up)) as Checkpoint;

                ch.gameObject.name = "Checkpoint " + i;

                if (trackSettings.parentTo != null)
                    ch.transform.SetParent(trackSettings.parentTo, true);

                checkpoints.Add(ch);

                progress += trackSettings.checkpointProgressDelta;
                ++i;
            }

            trackSettings.Checkpoints = checkpoints.ToArray();
        }

        float speedLimit = 200.0f;
        float acceleration = 15000.0f;
        CarMaterialManager.E_CAR_MATERIAL playerMat = CarMaterialManager.E_CAR_MATERIAL.GREEN_AND_MEAN;

        //Getting RaceSettings, if present.
        GameObject raceSettingsGO = GameObject.FindGameObjectWithTag("RaceSetting");
        RaceSettings raceSettings = null;
        if (raceSettingsGO != null)
            raceSettings = raceSettingsGO.GetComponent<RaceSettings>();

        if (raceSettings != null)
        {
            Debug.Log("GameManager : Found a Race Setting !");

            GenerateCarsAndPlayer(raceSettings.iaNumber, raceSettings.speedSpetting);
            switch (raceSettings.speedSpetting)
            {
                case RaceSettings.E_SPEED_SETTING.LOW:
                    speedLimit = 50;
                    acceleration = 7500;
                    break;
                case RaceSettings.E_SPEED_SETTING.MEDIUM:
                    speedLimit = 125;
                    acceleration = 12500;
                    break;
                case RaceSettings.E_SPEED_SETTING.HIGH:
                    speedLimit = 200;
                    acceleration = 15000;
                    break;
                case RaceSettings.E_SPEED_SETTING.VERYHIGH:
                    speedLimit = 300;
                    acceleration = 18000;
                    break;
            }

            lapsToWin = raceSettings.lapCount;
            playerMat = raceSettings.playerMat;
        }
        else
        {
            Debug.Log("GameManager : No RaceSettings found, initializing hard coded test settings.");
            GenerateCarsAndPlayer(2, RaceSettings.E_SPEED_SETTING.HIGH);
        }

        //Set some settings to everyone
        foreach (var ia in iaCars)
        {
            HoverCarControl cc = ia.GetComponent<HoverCarControl>();
            cc.maxVelocity = speedLimit;
            cc.forwardAcceleration = acceleration;
            CarMaterialManager materialManager = ia.GetComponent<CarMaterialManager>();
            if (materialManager != null)
                materialManager.ApplyMaterial((CarMaterialManager.E_CAR_MATERIAL)Random.Range(0, CarMaterialManager.E_CAR_MATERIALS.count));
        }

        HoverCarControl pcc = playerCar.GetComponent<HoverCarControl>();
        pcc.maxVelocity = speedLimit;
        pcc.forwardAcceleration = acceleration;
        CarMaterialManager pmaterialManager = playerCar.GetComponent<CarMaterialManager>();
        if (pmaterialManager != null)
            pmaterialManager.ApplyMaterial(playerMat);

        GameObject highScoresManagerGameObject = GameObject.FindGameObjectWithTag("HighScoresManager");
        if (highScoresManagerGameObject != null)
            highScoresManager = highScoresManagerGameObject.GetComponent<HighScoreManager>();
    }

    public void OnRacerLapValidated(OnLapValidatedArgs args)
    {
        if (args.tracker.Laps >= lapsToWin && rankings.Find(x => x == args.tracker) == null)
        {
            rankings.Add(args.tracker);

            if (onRacerFinishedRace != null)
                onRacerFinishedRace.Invoke(new OnRacerFinishedRaceArgs(args.tracker));

            args.tracker.gameObject.GetComponent<RaceTimer>().stopTimer();
            UpdateScoresScreen();

            if (args.tracker.gameObject.tag == "Player" || rankings.Count >= iaCars.Length  + 1 - 1) // +1 for player
            {
                GameObject playerCar = null;
                if (args.tracker.gameObject.tag == "Player")
                    playerCar = args.tracker.gameObject;
                else if (rankings.Count >= iaCars.Length - 1 + 1) // +1 for player
                    playerCar = GameObject.FindGameObjectWithTag("Player");

                Destroy(playerCar.GetComponent<PlayerInput>());
                IAController controller = playerCar.AddComponent<IAController>();
                controller.dotMultiplier = 18.0f;
                controller.progressAnticipation = 0.017f;
                controller.inputSystem = playerCar.AddComponent<IAInput>();
                controller.tracker = playerCar.GetComponent<ProgressTracker>();

                CarLink link = playerCar.GetComponent<CarLink>();
                link.inputSystem = controller.inputSystem;

                HoverCarControl hvcontroller = playerCar.GetComponent<HoverCarControl>();
                hvcontroller.turnStrength = 2550;

                GameState = E_GAMESTATE.END;
            }
        }
    }

    private void GenerateCarsAndPlayer(int iaNumber, RaceSettings.E_SPEED_SETTING speedSetting)
    {
        List<int> placesTaken = new List<int>();

        int pos = 0;

        //Generate IAs
        iaCars = new ProgressTracker[iaNumber];
        for (int i = 0; i < iaNumber; i++)
        {
            do
                pos = Random.Range(0, 7);
            while (placesTaken.Contains(pos));

            GameObject iaInstance = Instantiate(trackSettings.iaPrefab, 
                trackSettings.spawnPoints[pos].position, trackSettings.spawnPoints[pos].rotation) as GameObject;

            ProgressTracker ptIA = iaInstance.GetComponent<ProgressTracker>();
            ptIA.Track = trackSettings.track;
            ptIA.onLapValidated.RemoveAllListeners();
            ptIA.onLapValidated.AddListener(OnRacerLapValidated);

            iaCars[i] = ptIA;

            placesTaken.Add(pos);
        }

        pos = 0;

        //Generate Player
        do
            pos++;
        while (placesTaken.Contains(pos));

        GameObject playerInstance = Instantiate(trackSettings.playerPrefab, 
            trackSettings.spawnPoints[pos].position, trackSettings.spawnPoints[pos].rotation) as GameObject;

        ProgressTracker pt = playerInstance.GetComponent<ProgressTracker>();
        pt.Track = trackSettings.track;
        pt.onLapValidated.RemoveAllListeners();
        pt.onLapValidated.AddListener(OnRacerLapValidated);

        playerCar = pt;
    }

    private void OnDrawGizmos()
    {
        if (showTrackDebugCursor && trackSettings.track != null)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawSphere(trackSettings.track.GetPoint(trackDebugCursorPosition), 5.0f);
        }
    }

    private void UpdateScoresScreen()
    {
        highScoresManager.CheckScores();

        GameObject Camera = GameObject.Find("Camera");
        if (Camera != null)
        {
            Transform canvas = Camera.transform.FindChild("Canvas");
            if (canvas != null)
            {
                Transform ui = canvas.transform.FindChild("UI");
                if (ui != null)
                {
                    Text text = ui.FindChild("Timers").gameObject.GetComponent<Text>();
                    text.text = "";

                    for (uint i = 0; i < highScoresManager.GetCurrentRaceScores().Count; ++i)
                        text.text += (i + 1).ToString() + ".  " + highScoresManager.GetCurrentRaceScores()[(int)i].name + "   " + highScoresManager.GetCurrentRaceScores()[(int)i].GetScoreAsString() + "\n";
                }
            }
        }
    }
}


[System.Serializable]
public class OnGameStateChanged : UnityEngine.Events.UnityEvent<OnGameStateChangedArgs> { }

[System.Serializable]
public class OnGameStateChangedArgs
{
    public E_GAMESTATE newState;

    public OnGameStateChangedArgs(E_GAMESTATE state)
    {
        newState = state;
    }
}

public enum E_GAMESTATE
{
    START = 0,
    GAME,
    END
}

[System.Serializable]
public class TrackSettings
{
    [Range(0.0001f, 1.0f)]
    public float checkpointProgressDelta = 0.05f;
    public Transform parentTo = null;

    public BezierSpline track = null;

    [SerializeField]
    private Checkpoint checkpointPrefab = null;
    public Checkpoint CheckpointPrefab { get { return checkpointPrefab; } set { checkpointPrefab = value; } }

    private Checkpoint[] checkpoints = new Checkpoint[0];
    public Checkpoint[] Checkpoints { set { checkpoints = value; } get { return checkpoints; } }

    [SerializeField]
    private List<BaseUsableObject> itemsLibrary = new List<BaseUsableObject>();
    public List<BaseUsableObject> ItemsLibrary { get { return itemsLibrary; } }

    public GameObject playerPrefab = null;
    public GameObject iaPrefab = null;
    public Transform[] spawnPoints = new Transform[0];

    public ProgressTracker.NoTPZone[] noTpZones = new ProgressTracker.NoTPZone[0];
    public bool spawnBeforeStartLine = false;

    public float progressTrackersPrecision = 0.01f;
    public float iaAnticipation = 0.017f;
}

[System.Serializable]
public class OnRacerFinishedRace : UnityEngine.Events.UnityEvent<OnRacerFinishedRaceArgs> { }

[System.Serializable]
public class OnRacerFinishedRaceArgs
{
    public ProgressTracker tracker = null;

    public OnRacerFinishedRaceArgs(ProgressTracker t)
    {
        tracker = t;
    }
}