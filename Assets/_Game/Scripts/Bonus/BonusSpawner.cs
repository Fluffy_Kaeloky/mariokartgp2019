﻿using UnityEngine;
using System.Collections;

public class BonusSpawner : MonoBehaviour
{
    [SerializeField]
    private float respawnWaitTime = 10f;
    private float timer = 0f;
    private MeshRenderer meshRdr = null;
    private BoxCollider boxCollider = null;


    private void Start()
    {
        enabled = false;
        meshRdr = GetComponentInChildren<MeshRenderer>();
        boxCollider = GetComponentInChildren<BoxCollider>();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Vehicle"))
        {
            UsableObjectLauncher objLauncher = collider.gameObject.GetComponentInParent<UsableObjectLauncher>();
            if (objLauncher == null)
                return;

            GiveRandomObject(objLauncher);
            DisableAndHide();
            enabled = true;
        }
    }

    private void GiveRandomObject(UsableObjectLauncher objLauncher)
    {
        if (objLauncher.UsableObject != null || objLauncher.IsActualObjectTripple == false)
            return;

        int random = Random.Range(0, GameManager.ItemList.Count); 
        objLauncher.UsableObject = GameManager.ItemList[random];

        Debug.Log("Random object given : " + objLauncher.UsableObject);
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer > respawnWaitTime)
        {
            enabled = false;
            timer = 0f;
            EnableAndShow();
        }
    }

    private void DisableAndHide()
    {
        if (meshRdr != null && boxCollider != null)
        {
            meshRdr.enabled = false;
            boxCollider.enabled = false;
        }
    }

    private void EnableAndShow()
    {
        if (meshRdr != null && boxCollider != null)
        {
            meshRdr.enabled = true;
            boxCollider.enabled = true;
        }
    }
}
