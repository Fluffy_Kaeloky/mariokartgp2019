﻿using UnityEngine;
using System.Collections;

public class RedShell : GreenShell
{
    [SerializeField]
    private float forcePower = 300f;
    [SerializeField]
    private float startWaitTime = 2f;
    private ProgressTracker progressTracker = null;
    private Transform target = null;
    private Rigidbody rb = null;
    private bool hasFocus = false;
    private float startTimer = 0f;
    

    private void Start()
    {
        progressTracker = GetComponent<ProgressTracker>();
        rb = GetComponent<Rigidbody>();

        if (progressTracker != null)
            progressTracker.Track = GameManager.Instance.TrackSettings.track;

        if (isChildOfVehicle)
            DisableSelfAI();
    }

    protected override void Update()
    {
        base.Update();
        startTimer += Time.deltaTime;
    }

    // Do not Delete
    protected override void FixedUpdate() { }

    protected override void OnCollisionEnter(Collision collision)
    {
        if (startTimer >= startWaitTime)
            base.OnCollisionEnter(collision);

        if (collision.gameObject.layer == LayerMask.NameToLayer("WallCollider"))
            Destroy(gameObject);
    }
    
    private void OnTriggerEnter(Collider collider)
    {
        if (startTimer < startWaitTime || hasFocus || isChildOfVehicle)
            return;

        if (collider.transform.root.gameObject.layer == LayerMask.NameToLayer("Vehicle"))// && collider.gameObject.transform.root.tag != "Player")
        {
            DisableSelfAI();
            hasFocus = true;

            target = collider.gameObject.transform.root.transform;
            MoveToTarget();
        }
    }

    private void DisableSelfAI()
    {
        progressTracker.enabled = false;
        GetComponent<IAInput>().enabled = false;
        GetComponent<IAController>().enabled = false;
        GetComponent<CarLink>().enabled = false;
        GetComponent<HoverCarControl>().enabled = false;
    }

    private void MoveToTarget()
    {
        rb.mass = 1f;
        rb.drag = 0f;
        rb.angularDrag = 0.05f;

        transform.LookAt(target.position);
        rb.AddForce(transform.forward * forcePower, ForceMode.Impulse);
    }
}
