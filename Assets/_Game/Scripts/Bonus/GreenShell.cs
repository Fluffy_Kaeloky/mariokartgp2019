﻿using UnityEngine;


public class GreenShell : BaseGroundedObject {

    [SerializeField]
    private float timeBeforeSelfDestroy;
    private float baseY;
    private float baseDistanceToGround = 0;

    [SerializeField]
    private float effectDuration = 1f;
    private float timer = 0f;
    private Animator animator;
    private CarLink carLink;
    private PersonalEffects carPersonalEffects = null;
    private GameObject owner = null;
    protected bool isChildOfVehicle = false;
    
    public bool IsChildOfVehicle { get { return isChildOfVehicle; } set { isChildOfVehicle = value; } }
    public GameObject Owner { get { return owner; } set { owner = value; } }


    private void Start()
    {
        this.baseY = this.transform.position.y;
        if (IsChildOfVehicle == false)
            Destroy(gameObject, timeBeforeSelfDestroy);
    }


    protected virtual void FixedUpdate()
    {
        if (isChildOfVehicle == false)
            UpdatePositionFromFloor();
    }

    protected virtual void Update()
    {
        IncreaseTimerEffect();
    }
    
    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (owner != null)
        {
            if (collision.transform.root.name == owner.name && isChildOfVehicle)
                return;
        }

        if (LayerMask.LayerToName(collision.gameObject.layer) == "Vehicle")
        {
            carPersonalEffects = collision.gameObject.GetComponent<PersonalEffects>();
            if (carPersonalEffects.IsInvicible == false)
                ApplyEffect(collision.gameObject);
        }

        if (LayerMask.LayerToName(collision.gameObject.layer) == "GroundedObject")
        {
            if (collision.gameObject.GetComponent<BaseGroundedObject>().CanBeDestroyed)
            {
                Destroy(gameObject);
                Destroy(collision.gameObject);
            }
        }
    }

    public void ApplyEffect(GameObject vehicle)
    {
        if (carPersonalEffects == null)
            carPersonalEffects = vehicle.GetComponent<PersonalEffects>();

        carPersonalEffects.IsInvicible = true;
        carLink = vehicle.GetComponent<CarLink>();

        DisableVehicle(vehicle);
        DoVehicleAnimation(vehicle);
        DisableAndHide();
    }

    private void UpdatePositionFromFloor()
    {
        RaycastHit hit;

        if (Physics.Raycast(this.transform.position, Vector3.down, out hit))
        {
            if (this.baseDistanceToGround == 0)
                this.baseDistanceToGround = hit.distance;
            else if (hit.distance != this.baseDistanceToGround)
                this.baseY += this.baseDistanceToGround - hit.distance;
        }

        Vector3 new_pos = this.transform.position;
        new_pos.y = this.baseY;

        this.transform.position = new_pos;

        Quaternion shellRotation = Quaternion.FromToRotation(Vector3.down, hit.normal);
        this.transform.rotation = shellRotation;
    }

    private void DoVehicleAnimation(GameObject vehicle)
    {
        animator = vehicle.GetComponent<Animator>();
        if (animator == null)
            return;

        animator.SetTrigger("GreenShellTrigger");
    }

    private void DisableVehicle(GameObject vehicle)
    {
        timer = 0f;

        if (carLink != null)
            carLink.enabled = false;
    }

    private void IncreaseTimerEffect()
    {
        if (carLink != null)
            timer += Time.deltaTime;

        if (timer >= effectDuration)
        {
            if (carLink != null)
                carLink.enabled = true;

            carPersonalEffects.IsInvicible = false;
            Destroy(gameObject);
        }
    }
}
