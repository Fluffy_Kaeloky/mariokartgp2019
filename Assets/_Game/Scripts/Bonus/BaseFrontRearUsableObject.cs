﻿using UnityEngine;
using System.Collections;

public abstract class BaseFrontRearUsableObject : BaseUsableObject, IUsableObject, ISecondaryUsableObject
{
    public virtual void Use(GameObject obj, Transform spawnTrans, Transform forwardDirection, Vector3 carVelocity)
    {
        obj.transform.position = spawnTrans.position;
        Rigidbody rigidbody = obj.GetComponentInChildren<Rigidbody>();
        if (rigidbody == null)
            return;

        obj.transform.LookAt(forwardDirection);
        rigidbody.AddForce(carVelocity + obj.transform.forward * 30, ForceMode.Impulse);
        obj.transform.rotation = spawnTrans.rotation;
    }

    public virtual void SecondaryUse(GameObject obj, Transform spawnTrans)
    {
        obj.transform.position = spawnTrans.position;
        obj.GetComponent<Rigidbody>().isKinematic = true;
    }

    public virtual void UseTrippleObject(GameObject vehicle)
    {
        TripeShellRotator shellRotator = vehicle.GetComponentInChildren<TripeShellRotator>();

        if (shellRotator != null)
            shellRotator.AddShells(prefab);
    }
}
