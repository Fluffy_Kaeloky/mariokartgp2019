﻿using UnityEngine;
using System.Collections;

public enum E_DEFAULT_THROW_DIRECTION
{
    FRONT = 0,
    REAR
}


[System.Serializable]
public class BaseUsableObject : ScriptableObject
{
    [SerializeField]
    protected GameObject prefab;
    [SerializeField]
    protected bool isNotThrowable = false;
    [SerializeField]
    protected bool isTripple = false;

    public GameObject Prefab { get { return prefab; } }
    public bool IsNotThrowable { get { return isNotThrowable; } }
    public bool IsTripple { get { return isTripple; } set { isTripple = value; } }
}
