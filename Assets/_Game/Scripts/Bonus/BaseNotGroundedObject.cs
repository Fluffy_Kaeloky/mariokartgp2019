﻿using UnityEngine;


public class BaseNotGroundedObject : MonoBehaviour
{
    public virtual void DoAction(GameObject vehicle)
    {
        Destroy(gameObject);
    }
}
