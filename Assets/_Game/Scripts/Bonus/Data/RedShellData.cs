﻿using UnityEngine;
using System.Collections;

public class RedShellData : BaseFrontRearUsableObject
{
    public override void Use(GameObject obj, Transform trans, Transform lookAt, Vector3 velocity)
    {
        obj.transform.position = trans.position;
        Rigidbody rigidbody = obj.GetComponent<Rigidbody>();

        if (rigidbody == null)
            return;

        rigidbody.isKinematic = false;
        rigidbody.AddForce((lookAt.position - trans.position + velocity) * 20 * 50, ForceMode.Impulse);
    }

    public override void SecondaryUse(GameObject obj, Transform trans)
    {
        Rigidbody rigidbody = obj.GetComponent<Rigidbody>();

        if (rigidbody == null)
            return;

        rigidbody.isKinematic = true;

        obj.transform.position = trans.position;
    }
}
