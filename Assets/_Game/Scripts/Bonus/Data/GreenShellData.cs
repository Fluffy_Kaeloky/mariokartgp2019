﻿using UnityEngine;
using System.Collections;

public class GreenShellData : BaseFrontRearUsableObject {

    public override void Use(GameObject obj, Transform trans, Transform lookAt, Vector3 velocity)
    {
        obj.transform.position = trans.position;
        Rigidbody rigidbody = obj.GetComponent<Rigidbody>();

        if (rigidbody == null)
            return;

        rigidbody.isKinematic = false;
        rigidbody.useGravity = true;
        rigidbody.AddForce((lookAt.position - trans.position) * 20, ForceMode.Impulse);
    }

    public override void SecondaryUse(GameObject obj, Transform trans)
    {
        Rigidbody rigidbody = obj.GetComponent<Rigidbody>();

        if (rigidbody == null)
            return;

        rigidbody.isKinematic = true;
        rigidbody.useGravity = false;

        obj.transform.position = trans.position;
    }
}
