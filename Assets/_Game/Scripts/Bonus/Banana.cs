﻿ using UnityEngine;
using System.Collections.Generic;

public class Banana : BaseGroundedObject
{
    [SerializeField]
    private float effectDuration = 2f;
    private float timer = 0f;
    private Animator animator = null;
    private CarLink carLink;
    private Rigidbody vehicleRb = null;
    private Quaternion vehicleRotation;
    private PersonalEffects carPersonnalEffects = null;


    private void Start()
    {
        enabled = false;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= effectDuration)
        {
           // ResetVehicleObjectRotation("Hover9K_v2.0");
          //  ResetVehicleObjectRotation("Effects");
            carLink.enabled = true;
            carPersonnalEffects.IsInvicible = false;

            Destroy(gameObject);
        }
    }

    public override void DoAction(Collider vehicleCollider)
    {
        animator = vehicleCollider.transform.root.GetComponent<Animator>();
        carLink = vehicleCollider.gameObject.GetComponentInParent<CarLink>();
        HoverCarControl hoverCarCtrl = vehicleCollider.gameObject.GetComponentInParent<HoverCarControl>();
        vehicleRb = vehicleCollider.transform.root.GetComponent<Rigidbody>();
        carPersonnalEffects = vehicleCollider.transform.root.GetComponent<PersonalEffects>();
        
        if (animator == null || carLink == null || hoverCarCtrl == null || vehicleRb == null || carPersonnalEffects == null)
            return;

         if (carPersonnalEffects.IsInvicible)
            return;

        animator.SetTrigger("BananaTrigger");
        vehicleRotation = vehicleRb.rotation;
        enabled = true;
        carLink.enabled = false;
        carPersonnalEffects.IsInvicible = true;

        DisableAndHide();
    }

    private void ResetVehicleObjectRotation(string objName)
    {
        Transform obj = animator.transform;
        int idx = 0;

        while (obj.name != objName || idx > animator.transform.childCount)
        {
            obj = animator.transform.GetChild(idx);
            ++idx;
        }

        obj.transform.rotation = vehicleRotation;
    }
}
