﻿using UnityEngine;


public class BaseGroundedObject : MonoBehaviour
{
    [SerializeField]
    protected bool canBeDestroyed = false;
    public bool CanBeDestroyed { get { return canBeDestroyed; } }

    public OnGroundedObjectFired onGroundedObjFired;


    private void OnTriggerEnter(Collider collider) 
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("MapCollider")) // Place the object on the ground when launched
        {
            GetComponent<Rigidbody>().isKinematic = true;
            Animator animator = GetComponentInChildren<Animator>();
            if (animator != null)
                animator.enabled = true;
        }
        else if (collider.gameObject.layer == LayerMask.NameToLayer("Vehicle"))
            DoAction(collider);
        else if (collider.gameObject.layer == LayerMask.NameToLayer("GroundedObject"))
            DoGroundedAction(collider.gameObject);
    }

    public virtual void DoAction(Collider collider)
    {
        Debug.Log("Object (Bonus) destroyed.");
        Destroy(gameObject);
    }

    protected virtual void DoGroundedAction(GameObject gameObject)
    {
        Destroy(gameObject);
        DestroyObject(gameObject);
    }

    protected void DisableAndHide()
    {
        MeshRenderer meshRdr = GetComponent<MeshRenderer>();
        Collider collider = GetComponent<Collider>();

        if (meshRdr != null && collider != null)
        {
            meshRdr.enabled = false;
            collider.enabled = false;
        }
        else
        {
            meshRdr = GetComponentInChildren<MeshRenderer>();
            collider = GetComponentInChildren<Collider>();

            if (meshRdr != null && collider != null)
            {
                meshRdr.enabled = false;
                collider.enabled = false;
            }
        }
    }
}

[System.Serializable]
public class OnGroundedObjectFired : UnityEngine.Events.UnityEvent<OnGroundedObjectFiredArgs> { }

[System.Serializable]
public class OnGroundedObjectFiredArgs
{
    public BaseGroundedObject objectUsed = null;
    
    public OnGroundedObjectFiredArgs(BaseGroundedObject o)
    {
        objectUsed = o;
    }
}
