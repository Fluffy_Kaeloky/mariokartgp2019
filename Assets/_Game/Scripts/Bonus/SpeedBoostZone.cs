﻿using UnityEngine;
using System.Collections;

public class SpeedBoostZone : BaseGroundedObject
{
    [SerializeField]
    private float speedBoost = 3000f;


   private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("Vehicle"))
        {
            if (collider.transform.root.GetComponent<UsableObjectLauncher>().IsActualObjectTripple)
                return;

            Rigidbody vehicleRb = collider.transform.root.GetComponent<Rigidbody>();
            if (vehicleRb != null)
            {
                vehicleRb.AddForce(vehicleRb.transform.forward * speedBoost, ForceMode.Impulse);
                CallEvent();
            }
        }
    }

    private void CallEvent()
    {
        UsableObjectLauncher launcher = GameManager.Instance.PlayerCar.GetComponent<UsableObjectLauncher>();

        if (launcher != null)
            launcher.DoGroundedObjectEvent(this);
    }
}