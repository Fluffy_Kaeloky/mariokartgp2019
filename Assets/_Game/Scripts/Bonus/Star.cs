﻿using UnityEngine;
using System.Collections;

public class Star : BaseNotGroundedObject
{
    [SerializeField]
    private float effectDuration = 4f;
    [SerializeField]
    private float velocityBonus = 300f;
    private float timer = 0f;
    private float vehicleMaxVelocity = 0f;
    private HoverCarControl hoverCarCtrl = null;
    private PersonalEffects vehicleEffects = null;


    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= effectDuration)
        {
            hoverCarCtrl.maxVelocity = vehicleMaxVelocity;
            vehicleEffects.IsInvicible = false;
            vehicleEffects.CanHurtEnemies = false;

            Destroy(gameObject); 
        }
    }

    public override void DoAction(GameObject vehicle)
    {
        Rigidbody vehicleRb = vehicle.GetComponent<Rigidbody>();
        hoverCarCtrl = vehicleRb.GetComponent<HoverCarControl>();
        vehicleEffects = vehicle.GetComponent<PersonalEffects>();

        if (vehicleRb == null || hoverCarCtrl == null || vehicleEffects == null)
            return;

        vehicleMaxVelocity = hoverCarCtrl.maxVelocity;
        hoverCarCtrl.maxVelocity = hoverCarCtrl.maxVelocity + velocityBonus;

        vehicleEffects.IsInvicible = true;
        vehicleEffects.CanHurtEnemies = true;
    }
}
