﻿using UnityEngine;
using System.Collections;

public class FakeBox : BaseGroundedObject
{
    [SerializeField]
    private float effectDuration = 3f;
    private float timer = 0f;
    private CarLink carLink = null;
    private Animator animator = null;
    private PersonalEffects vehicleEffects = null;


    private void Start()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        enabled = false;
        animator = GetComponentInChildren<Animator>();

        if (animator != null)
            animator.enabled = false;
    }

    public override void DoAction(Collider vehicleCollider)
    {
        vehicleEffects = vehicleCollider.transform.root.GetComponent<PersonalEffects>();
        if (vehicleEffects.IsInvicible)
            return;

        carLink = vehicleCollider.gameObject.GetComponentInParent<CarLink>();
       
        if (carLink != null)
        {
            timer = 0f;
            carLink.enabled = false;
            enabled = true;

            vehicleEffects.IsInvicible = true;
            DisableAndHide();
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= effectDuration)
        {
            if (carLink != null)
                carLink.enabled = true;

            vehicleEffects.IsInvicible = false;
            Destroy(gameObject);
        }
    }

   
}
