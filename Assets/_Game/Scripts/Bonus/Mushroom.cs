﻿using UnityEngine;
using System.Collections;

public class Mushroom : BaseNotGroundedObject
{
    [SerializeField]
    private float forceBonus = 500f;


    public override void DoAction(GameObject vehicle)
    {
        Rigidbody vehicleRb = vehicle.GetComponent<Rigidbody>();
        if (vehicleRb == null)
            return;

        vehicleRb.AddForce(vehicleRb.transform.forward * forceBonus, ForceMode.Impulse);
        Destroy(gameObject);
    }
}
