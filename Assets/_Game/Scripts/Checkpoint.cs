﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Checkpoint : MonoBehaviour
{
    public OnCheckpointPass onCheckpointPass;

    private void OnTriggerEnter(Collider other)
    {
        ProgressTracker tracker = null;
        if ((tracker = other.GetComponentInParent<ProgressTracker>()) != null)
        {
            if (onCheckpointPass != null)
                onCheckpointPass.Invoke(new OnCheckpointPassArgs(this, tracker));
        }
    }
}

[System.Serializable]
public class OnCheckpointPass : UnityEvent<OnCheckpointPassArgs> { }

[System.Serializable]
public class OnCheckpointPassArgs
{
    public Checkpoint sender = null;
    public ProgressTracker tracker = null;

    public OnCheckpointPassArgs(Checkpoint sender, ProgressTracker tracker)
    {

        this.sender = sender;
        this.tracker = tracker;
    }
}
