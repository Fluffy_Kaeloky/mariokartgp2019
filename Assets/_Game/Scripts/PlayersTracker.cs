﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class PlayersTracker : MonoBehaviour
{
    public OnPlayerProgressOrderChanged onPlayerProgressOrderChanged;

    private ProgressTracker[] trackers = new ProgressTracker[0];

    private System.Comparison<ProgressTracker> sortFunc = delegate (ProgressTracker a, ProgressTracker b)
        {
            if (a.TotalProgress < b.TotalProgress)
                return 1;
            else if (a.TotalProgress > b.TotalProgress)
                return -1;
            return 0;
        };

    private void Start()
    {
        List<ProgressTracker> list = new List<ProgressTracker>(GameManager.Instance.IACars);
        list.Add(GameManager.Instance.PlayerCar);
        trackers = list.ToArray();

        System.Array.Sort(trackers, sortFunc);

        onPlayerProgressOrderChanged.Invoke();
    }

    private void Update()
    {
        for (int i = 0; i < trackers.Length - 1; i++)
        {
            if (trackers[i].TotalProgress < trackers[i + 1].TotalProgress)
            {
                System.Array.Sort(trackers, sortFunc);

                if (onPlayerProgressOrderChanged != null)
                    onPlayerProgressOrderChanged.Invoke();

                break;
            }
        }
    }

    public int GetPosition(ProgressTracker tracker)
    {
        for (int i = 0; i < trackers.Length; i++)
        {
            if (trackers[i] == tracker)
                return i + 1;
        }

        return -1;
    }
}

[System.Serializable]
public class OnPlayerProgressOrderChanged : UnityEngine.Events.UnityEvent { }
