﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

public class HighScoreSerializer {

    private FileStream stream = null;
    XmlSerializer xml = null;
    private string filename = "";

    public HighScoreSerializer(string filename)
    {
        this.filename = filename;
    }


    public void SerializeObject(List<HighScore> highscoreToSerialize)
    {
        this.stream = File.Open(this.filename, FileMode.Create);
        this.xml = new XmlSerializer(typeof(List<HighScore>));

        this.xml.Serialize(stream, highscoreToSerialize);

        stream.Close();
    }

    public List<HighScore> DeserializeHighScore()
    {
        if (File.Exists(this.filename))
        {
            this.stream = File.Open(this.filename, FileMode.Open);
            this.xml = new XmlSerializer(typeof(List<HighScore>));

            List<HighScore> highscoreList = (List<HighScore>)this.xml.Deserialize(stream);

            stream.Close();

            return highscoreList;
        }

        return null;
    }

    public void DeleteFile()
    {
        if (File.Exists(this.filename))
            File.Delete(this.filename);
    }
}