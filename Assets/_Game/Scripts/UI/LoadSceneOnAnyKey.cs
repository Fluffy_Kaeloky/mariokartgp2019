﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnAnyKey : MonoBehaviour
{
    public string sceneToLoad = "";

    public void Update()
    {
        if (Input.anyKeyDown && sceneToLoad != null)
            SceneManager.LoadScene(sceneToLoad);
    }
}
