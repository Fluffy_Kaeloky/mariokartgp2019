﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class HighScoresMenu : MonoBehaviour {

    [SerializeField]
    private Text rankText;
    [SerializeField]
    private Text nicknameText;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text noScoreText;
    [SerializeField]
    private RectTransform buttonPanel;
    [SerializeField]
    private DisplayManager displayManager;
    [SerializeField]
    private Button resetScoresButton;
    [SerializeField]
    private GameObject defaultSelectedObject;

    private List<HighScore> highScoreList = null;

	void Start () {
        
    }

    private void GetHighScoresListFromHighScoreManager()
    {
        GameObject highScoreManagerGameObject = GameObject.FindGameObjectWithTag("HighScoresManager");
        if (highScoreManagerGameObject != null)
            highScoreList = highScoreManagerGameObject.GetComponent<HighScoreManager>().GetHighScoresList();

        DisplayScores();
    }

    private void DisplayScores()
    {
        ResetAllText();

        if (this.highScoreList == null || this.highScoreList.Count == 0)
            this.noScoreText.gameObject.SetActive(true);
        else
            for (uint i = 0; i < this.highScoreList.Count ; ++i)
            {
                this.rankText.text += i+1 + "\n";
                this.nicknameText.text += this.highScoreList[(int)i].name + "\n";
                this.scoreText.text += this.highScoreList[(int)i].GetScoreAsString() + "\n";
            }
    }

    private void ResetAllText()
    {
        this.rankText.text = "";
        this.nicknameText.text = "";
        this.scoreText.text = "";
    }

    public void OnMainMenuClick()
    {
        if (displayManager == null)
            return;

        ResetAllText();

        this.noScoreText.gameObject.SetActive(false);
        this.resetScoresButton.gameObject.SetActive(false);
        this.buttonPanel.gameObject.SetActive(true);

        displayManager.OnMainMenuClick();
    }


    public GameObject GetDefaultSelectedObject()
    {
        return this.defaultSelectedObject;
    }


    public void OnLevelSelectedClick(Button button)
    {
        GameObject highScoreManagerGameObject = GameObject.FindGameObjectWithTag("HighScoresManager");
        if (highScoreManagerGameObject != null)
            highScoreManagerGameObject.GetComponent<HighScoreManager>().ReloadScores(button.GetComponent<TextAttached>().GetLevelName());

        GetHighScoresListFromHighScoreManager();

        if (resetScoresButton != null)
            resetScoresButton.gameObject.SetActive(true);
        if (buttonPanel != null)
            buttonPanel.gameObject.SetActive(false);

        EventSystem.current.SetSelectedGameObject(this.defaultSelectedObject.gameObject);
    }

    public void OnResetScoresClick()
    {
        GameObject highScoreManagerGameObject = GameObject.FindGameObjectWithTag("HighScoresManager");
        if (highScoreManagerGameObject != null)
            highScoreManagerGameObject.GetComponent<HighScoreManager>().DeleteCurrentScores();

        GetHighScoresListFromHighScoreManager();
    }

}
