﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class PositionDisplay : MonoBehaviour
{
    public Sprite[] positionSprites = new Sprite[0];
    public float lerpFactor = 5.0f;
    public Vector3 scaleOnChange = Vector3.one;

    private Vector3 targetScale = Vector3.zero;
    private Image image = null;
    private ProgressTracker target = null;

    private void Start()
    {
        target = GameManager.Instance.PlayerCar;

        targetScale = transform.localScale;
        image = GetComponent<Image>();
    }

    private void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, targetScale, lerpFactor * Time.deltaTime);
    }

    public void OnOrderChanged(PlayersTracker playerTracker)
    {
        if (!enabled)
            return;

        int newPos = playerTracker.GetPosition(target) - 1;
        if (newPos == -1)
            return;

        int arrayPos = Mathf.Min(newPos, positionSprites.Length);

        image.sprite = positionSprites[arrayPos];

        transform.localScale = scaleOnChange;
    }
}
