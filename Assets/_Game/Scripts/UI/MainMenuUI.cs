﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class MainMenuUI : MonoBehaviour {

    [SerializeField]
    private DisplayManager displayManager;
    [SerializeField]
    private uint maxNumberOfHighScores;
    [SerializeField]
    private GameObject defaultSelectedObject;

    private void Start()
    {
        SetUpHighScoresManager();
    }

    private void SetUpHighScoresManager()
    {
        GameObject highScoresManager = GameObject.FindGameObjectWithTag("HighScoresManager");
        if (highScoresManager != null)
            Destroy(highScoresManager);

        highScoresManager = new GameObject("HighScoresManager");
        highScoresManager.tag = "HighScoresManager";
        highScoresManager.AddComponent<HighScoreManager>();
        highScoresManager.GetComponent<HighScoreManager>().SetMaxNumberOfScores(maxNumberOfHighScores);
        DontDestroyOnLoad(highScoresManager);
    }

    public void OnNewGameClick()
    {
        displayManager.OnNewGameClick();
    }

    public void OnHighScoreClick()
    {
        displayManager.OnHighScoreClick();
    }

    public void OnQuitGameClick()
    {
        Application.Quit();
    }

    public GameObject GetDefaultSelectedObject()
    {
        return this.defaultSelectedObject;
    }
}
