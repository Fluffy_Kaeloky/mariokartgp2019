﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[Serializable]
public struct timer_struct
{
    public uint min;
    public float sec;
}

public class RaceTimer : MonoBehaviour {

    [SerializeField]
    private Text textTimer;
    private timer_struct timer;
    private bool hasStarted = false;
    private bool hasEnded = false;


    void Start()
    {
        this.textTimer = this.GetComponent<Text>();
        GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChange);
    }

    public RaceTimer()
    {
        this.timer.min = 0;
        this.timer.sec = 0f;
    }

    public RaceTimer(uint minute, float seconde)
    {
        this.timer.min = minute;
        this.timer.sec = seconde;

        this.hasStarted = false;
    }

    public RaceTimer(timer_struct timer)
    {
        this.timer = timer;
    }


    void Update()
    {
        UpdateTimer();
    }


    private void UpdateTimer()
    {
        if (!this.hasStarted)
            return;
        if (this.hasEnded)
            return;

        this.timer.sec += Time.deltaTime;

        FixTimer();
        DisplayTimer();
    }

    private void FixTimer()
    {
        if (this.timer.sec >= 60)
        {
            this.timer.min += 1;
            this.timer.sec -= 60;
        }
    }

    private void DisplayTimer()
    {
        if (this.textTimer != null)
            this.textTimer.text = "Time: " + GetTimerAsString();
    }


    public void startTimer()
    {
        this.hasStarted = true;
    }

    public void stopTimer()
    {
        this.hasEnded = true;
    }

    public void OnGameStateChange(OnGameStateChangedArgs args)
    {
        if (args.newState == E_GAMESTATE.GAME)
            startTimer();
//        else if (args.newState == E_GAMESTATE.END)
//            stopTimer();
    }

    public timer_struct GetTimerStruct()
    {
        return this.timer;
    }

    public string GetTimerAsString()
    {
        string displayMin = this.timer.min.ToString();
        string displaySec = this.timer.sec.ToString();

        if (this.timer.min < 10)
            displayMin = "0" + this.timer.min;
        if (this.timer.sec < 10)
            displaySec = "0" + this.timer.sec;

        if (displaySec.Length > 5)
            displaySec = displaySec.Remove(5);

        string stringTimer = displayMin + ":" + displaySec;

        return stringTimer;
    }

    public bool GetHasStarted()
    {
        return this.hasStarted;
    }

    public bool GetHasEnded()
    {
        return this.hasEnded;
    }
}
