﻿using UnityEngine;
using System.Collections;

public class RaceSettings : MonoBehaviour
{
    /// <summary>
    /// Between 1 and 7
    /// </summary>
    public int iaNumber = 1;
    public int lapCount = 3;

    public E_SPEED_SETTING speedSpetting = E_SPEED_SETTING.MEDIUM;

    public CarMaterialManager.E_CAR_MATERIAL playerMat = CarMaterialManager.E_CAR_MATERIAL.GREEN_AND_MEAN;

    public enum E_SPEED_SETTING
    {
        LOW,
        MEDIUM,
        HIGH,
        VERYHIGH
    }
}
