﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

 
public class DisplayObject : MonoBehaviour
{
    private UsableObjectLauncher objectLauncher = null;
    private Text text = null;
    private string defaultText = "Object : ";


    private void Start()
    {
        text = GetComponent<Text>();
        objectLauncher = GameManager.Instance.PlayerCar.gameObject.GetComponent<UsableObjectLauncher>();
    }

    private void Update()
    {
        if (objectLauncher.UsableObject == null)
        {
            text.text = defaultText;
            return;
        }
        
        text.text = defaultText + GetUsableObjectName(objectLauncher.UsableObject.name);
    }

    private string GetUsableObjectName(string objName)
    {
        if (objName.Contains("TrippleRed"))
            return "TripleRedShells";

        if (objName.Contains("TripleGreenShell"))
            return "TripleGreenShells";

        if (objName.Contains("GreenShell"))
            return "Green shell";

        if (objName.Contains("Banana"))
            return "Banana";

        if (objName.Contains("FakeBox"))
            return "Fake box";

        if (objName.Contains("Mushroom"))
            return "Mushroom";

        if (objName.Contains("Star"))
            return "Star";

        if (objName.Contains("RedShell"))
            return "RedShell";

        return "unknow";
    }

    public void OnGameStateChange(OnGameStateChangedArgs args)
    {
          if (args.newState == E_GAMESTATE.END)
            text.color = Color.white;
    }
}
