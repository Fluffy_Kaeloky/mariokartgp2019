﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeedTracker : MonoBehaviour
{
    public Image speedCounterBorder = null;
    public Text speedCounterValue = null;
    public string valuePostStr = " KP/H";

    public float maxVelocity = 100.0f;

    private Rigidbody target = null;

    private void Start()
    {
        target = GameManager.Instance.PlayerCar.gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float velocityAlongForward = Mathf.Abs(Vector3.Dot(target.velocity, target.transform.forward));
        speedCounterBorder.fillAmount = velocityAlongForward / maxVelocity;
        speedCounterValue.text = (int)velocityAlongForward + valuePostStr;
    }
}