﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class InGameUI : MonoBehaviour
{
    public bool autoRegister = true;
    public string triggerEndName = "End";
    public string fadeTriggerName = "Fade";
    public string fadeTimerName = "FadeTime";
    public string fadeBlackTimerName = "FadeBlackTime";

    private Animator animator = null;

    private void Start()
    {
        GameManager.Instance.onGameStateChanged.AddListener(OnGameStateChanged);
        animator = GetComponent<Animator>();
    }

    public void OnGameStateChanged(OnGameStateChangedArgs args)
    {
        if (args.newState == E_GAMESTATE.END)
            animator.SetTrigger(triggerEndName);
    }

    public void FadeScreen(float fadeTime, float blackTime)
    {
        animator.SetFloat(fadeTimerName, 1.0f / fadeTime);
        animator.SetFloat(fadeBlackTimerName, 1.0f / blackTime);
        animator.SetTrigger(fadeTriggerName);
    }
}
