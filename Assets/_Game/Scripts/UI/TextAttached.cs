﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextAttached : MonoBehaviour {

    [SerializeField]
    private Text textAttached;
    [SerializeField]
    private string text;
    [SerializeField]
    private string levelName;

    private void Start()
    {
        if (textAttached == null)
            return;

        this.textAttached.text = this.text;
    }

	public Text GetText()
    {
        return this.textAttached;
    }

    public string GetLevelName()
    {
        return this.levelName;
    }
}
