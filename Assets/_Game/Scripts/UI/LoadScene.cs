﻿using UnityEngine;
using System.Collections;

public class LoadScene : MonoBehaviour
{
    public string sceneToLoad = "MainMenu";

    public void LoadSceneCallback()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad);
    }
}
