﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class LapDisplay : MonoBehaviour
{
    private Text text = null;

    private void Start()
    {
        text = GetComponent<Text>();

        GameManager.Instance.PlayerCar.onLapValidated.AddListener(OnPlayerValidatedLap);

        text.text = "Lap : 1/" + GameManager.Instance.lapsToWin;
    }

    private void OnPlayerValidatedLap(OnLapValidatedArgs args)
    {
        text.text = "Lap : " + (args.tracker.Laps + 1) + "/" + GameManager.Instance.lapsToWin;
    }
}
