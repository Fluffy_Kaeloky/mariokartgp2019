﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;

[Serializable]
public class HighScore
{
    public string name { get; set; }
    public timer_struct score { get; set; }

    public HighScore()
    {
        this.name = "Unknown";
    }

    public HighScore(string new_name)
    {
        this.name = CheckSpaceInNickname(new_name);
    }

    public HighScore(string name, timer_struct score)
    {
        this.name = name;
        this.score = score;
    }

    public string GetScoreAsString()
    {
        string displayMin = this.score.min.ToString();
        string displaySec = this.score.sec.ToString();

        if (this.score.min < 10)
            displayMin = "0" + this.score.min;
        if (this.score.sec < 10)
            displaySec = "0" + this.score.sec;

        if (displaySec.Length > 5)
            displaySec = displaySec.Remove(5);

        string stringTimer = displayMin + ":" + displaySec;

        return stringTimer;
    }

    static public bool IsScoreBetter(HighScore highscore, HighScore highscoreToCheck)
    {
        if (highscoreToCheck.score.min < highscore.score.min)
            return true;
        else if (highscoreToCheck.score.min == highscore.score.min) 
            if (highscoreToCheck.score.sec < highscore.score.sec)
                return true;

        return false;
    }

    private string CheckSpaceInNickname(string nicknameToCheck)
    {
        for (uint i = 0; i < nicknameToCheck.Length; ++i)
        {
            if (nicknameToCheck[(int)i] != ' ')
                return RemoveSpaceFromNickname(nicknameToCheck);
        }

        return "Unknow";
    }

    private string RemoveSpaceFromNickname(string nicknameToModify)
    {
        uint index = 0;

        for (uint i = 0; i < nicknameToModify.Length; ++i)
        {
            if (nicknameToModify[(int)i] == ' ')
                ++index;
            else
                break;
        }

        if (index >= 1)
            return nicknameToModify.Substring((int)index);
        else
            return nicknameToModify;
    }
}
