﻿using UnityEngine;
using System.Collections;

public class ChangeGameState : MonoBehaviour
{
    public void DoChangeState(E_GAMESTATE state)
    {
        GameManager.Instance.GameState = state;
    }
}
