﻿using UnityEngine;
using System.Collections;

public class TrackingCamera : MonoBehaviour
{
    public float positionInterpolationFactor = 1.0f;
    public float rotationInterpolationFactor = 1.0f;

    public Transform target = null;
    public Transform focus = null;

    public Transform reverseTarget = null;
    public Transform reverseFocus = null;

    private void Start()
    {
        GameObject playerCar = GameManager.Instance.PlayerCar.gameObject;
        target = playerCar.transform.FindChild("Helpers/Camera/PositionTarget");
        focus = playerCar.transform.FindChild("Helpers/Camera/Focus");
        reverseTarget = playerCar.transform.FindChild("Helpers/Camera/ReversePositionTarget");
        reverseFocus = playerCar.transform.FindChild("Helpers/Camera/ReverseFocus");
    }

    private void FixedUpdate()
    {
        if (this.target != null && this.focus != null)
        {
            Transform target = this.target;
            Transform focus = this.focus;

            if (reverseTarget != null && reverseFocus != null && Input.GetButton("LookBehind"))
            {
                target = reverseTarget;
                focus = reverseFocus;
            }

            transform.position = Vector3.Lerp(transform.position, target.position, positionInterpolationFactor * Time.fixedDeltaTime);
            Quaternion startRotation = transform.rotation;
            transform.LookAt(focus);
            Quaternion targetRotation = transform.rotation;
            transform.rotation = Quaternion.Lerp(startRotation, targetRotation, rotationInterpolationFactor * Time.fixedDeltaTime);
        }
    }
}
