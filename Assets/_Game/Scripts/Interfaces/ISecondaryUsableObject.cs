﻿using UnityEngine;
using System.Collections;

public interface ISecondaryUsableObject
{
    void SecondaryUse(GameObject obj, Transform trans);
}