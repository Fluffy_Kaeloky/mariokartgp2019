﻿using UnityEngine;
using System.Collections;

public interface IUsableObject 
{
    void Use(GameObject obj, Transform trans, Transform forwardDirection, Vector3 carVelocity);
}
