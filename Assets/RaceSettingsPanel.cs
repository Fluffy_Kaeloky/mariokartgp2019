﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class RaceSettingsPanel : MonoBehaviour {

    [SerializeField]
    private Dropdown aiDropdown;
    [SerializeField]
    private Dropdown speedDropdown;
    [SerializeField]
    private Dropdown carColorDropdown;
    [SerializeField]
    private Text lapNumberText;
    [SerializeField]
    private InputField nicknameInputField;
    [SerializeField]
    private Text noSelectedLevelText;
    [SerializeField]
    private DisplayManager displayManager;
    [SerializeField]
    private GameObject defaultSelectedObject;

    private string levelChosen = null;
    private Text lastSelectedLevel = null;
    private RaceSettings raceSettings;
    private HighScoreManager highScoresManager;

    private void Start()
    {
        SetUpRaceSettings();
        SetUpHighScoresManager();
    }

    private void SetUpRaceSettings()
    {
        GameObject raceSettingsGO = GameObject.FindGameObjectWithTag("RaceSetting");
        if (raceSettingsGO != null)
            Destroy(raceSettingsGO);

        GameObject rsgo = new GameObject();
        rsgo.name = "RaceSettings";
        rsgo.tag = "RaceSetting";
        raceSettings = rsgo.AddComponent<RaceSettings>();
    }

    private void SetUpHighScoresManager()
    {
        GameObject highScoresManagerGameObject = GameObject.FindGameObjectWithTag("HighScoresManager");

        if (highScoresManagerGameObject != null)
            this.highScoresManager = highScoresManagerGameObject.GetComponent<HighScoreManager>();
    }

    public void OnStartRaceClick()
    {
        if (lastSelectedLevel == null)
        {
            noSelectedLevelText.gameObject.SetActive(true);
            return;
        }

        UpdateRaceSettings();
        UpdatePlayerNickname();
        DontDestroyOnLoad(raceSettings);
        SceneManager.LoadScene(levelChosen);
    }

    public void OnMainMenuClick()
    {
        displayManager.OnMainMenuClick();
    }

    public void OnLevelSelectClick(Button button)
    {
        TextAttached new_text = button.gameObject.GetComponent<TextAttached>();

        if (new_text == null)
            return;
        
        this.levelChosen = new_text.GetLevelName();

        if (lastSelectedLevel == null)
            this.lastSelectedLevel = new_text.GetText();
        else
        {
            this.lastSelectedLevel.color = Color.white;
            this.lastSelectedLevel = new_text.GetText();
        }

        noSelectedLevelText.gameObject.SetActive(false);

        lastSelectedLevel.color = Color.red;
        this.highScoresManager.ReloadScores(levelChosen);
    }

    private void UpdateRaceSettings()
    {
        if (speedDropdown != null)
            raceSettings.speedSpetting = (RaceSettings.E_SPEED_SETTING)speedDropdown.value;
        if (aiDropdown != null)
            raceSettings.iaNumber = Int32.Parse(aiDropdown.options[aiDropdown.value].text);
        if (lapNumberText != null)
            raceSettings.lapCount = Int32.Parse(this.lapNumberText.text);

        raceSettings.playerMat = GetCarColorFromDropdown();
    }

    private CarMaterialManager.E_CAR_MATERIAL GetCarColorFromDropdown()
    {
        if(carColorDropdown.options[carColorDropdown.value].text == "Green")
            return CarMaterialManager.E_CAR_MATERIAL.GREEN_AND_MEAN;
        else if (carColorDropdown.options[carColorDropdown.value].text == "Orange")
            return CarMaterialManager.E_CAR_MATERIAL.ORANGE;
        else if (carColorDropdown.options[carColorDropdown.value].text == "Purple")
            return CarMaterialManager.E_CAR_MATERIAL.PURPLE;
        else if (carColorDropdown.options[carColorDropdown.value].text == "Red")
            return CarMaterialManager.E_CAR_MATERIAL.RED;
        else
            return CarMaterialManager.E_CAR_MATERIAL.BLUE;
    }

    private void UpdatePlayerNickname()
    {
        if (this.highScoresManager == null || this.nicknameInputField == null)
            return;

        HighScore playerHighScore = new HighScore(this.nicknameInputField.text);
        this.highScoresManager.SetCurrentPlayerScore(playerHighScore);
    }

    public GameObject GetDefaultSelectedObject()
    {
        return this.defaultSelectedObject;
    }

    public void ModifyLapNumber(Slider slider)
    {
        int new_lap = (int)slider.value;

        if (new_lap < 1 || new_lap > 99)
            return;

        this.lapNumberText.text = new_lap.ToString();
    }
}
