﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class HighScoreManager : MonoBehaviour
{
    private uint maxNumberOfScore = 0;
    private List<HighScore> highScoresList;
    private string fileName = null;

    private HighScore currentPlayerScore = null;
    private List<HighScore> currentRaceScores = new List<HighScore>();
    private RaceTimer[] raceTimersCurrentRace = null;
    private List<string> carNames = new List<string>();


    private void LoadScores()
    {
        if (fileName == null)
            return;

        HighScoreSerializer serializer = new HighScoreSerializer(this.fileName);
        this.highScoresList = serializer.DeserializeHighScore();
    }

    public void AddScore(HighScore newHighScore)
    {
        if (this.highScoresList == null)
        {
            this.highScoresList = new List<HighScore>();
            this.highScoresList.Add(newHighScore);
        }
        else
        {
            for (uint i = 0; i < this.highScoresList.Count; ++i)
            {
                if (HighScore.IsScoreBetter(this.highScoresList[(int)i], newHighScore))
                {
                    this.highScoresList.Insert((int)i, newHighScore);
                    CheckHighScoresListSize();
                    break;
                }
                else if (this.highScoresList.Count < maxNumberOfScore && i == this.highScoresList.Count - 1)
                {
                    this.highScoresList.Add(newHighScore);
                    break;
                }
            }
        }

        HighScoreSerializer serializer = new HighScoreSerializer(fileName);
        serializer.SerializeObject(this.highScoresList);
    }


    private void CheckHighScoresListSize()
    {
        if (this.highScoresList.Count > this.maxNumberOfScore)
            this.highScoresList.RemoveRange((int)this.maxNumberOfScore, this.highScoresList.Count);
    }


    public void CheckScores()
    {
        if (raceTimersCurrentRace == null)
            return;

        for (uint i = 0; i < raceTimersCurrentRace.Length; ++i)
            if (raceTimersCurrentRace[(int)i] != null && raceTimersCurrentRace[(int)i].GetHasEnded())
            {
                this.currentRaceScores.Add(new HighScore(carNames[(int)i], raceTimersCurrentRace[(int)i].GetTimerStruct()));

                if (currentPlayerScore.name == carNames[(int)i])
                {
                    currentPlayerScore.score = raceTimersCurrentRace[(int)i].GetTimerStruct();
                    AddScore(currentPlayerScore);
                }

                raceTimersCurrentRace[(int)i] = null;
            }
    }

    public List<HighScore> GetHighScoresList()
    {
        return this.highScoresList;
    }

    public List<HighScore> GetCurrentRaceScores()
    {
        return this.currentRaceScores;
    }

    public void SetMaxNumberOfScores(uint number)
    {
        if (this.maxNumberOfScore == 0)
            this.maxNumberOfScore = number;
    }

    public void SetCurrentPlayerScore(HighScore newCurrentPlayer)
    {
        if (this.currentPlayerScore == null)
            this.currentPlayerScore = newCurrentPlayer;
    }

    public void OnLevelWasLoaded()
    {
        raceTimersCurrentRace = GameObject.FindObjectsOfType<RaceTimer>();

        Debug.Log(currentPlayerScore);

        uint aiCount = 0;

        for (uint i = 0; i < raceTimersCurrentRace.Length; ++i)
            if (raceTimersCurrentRace[(int)i].gameObject.tag != "Player")
            {
                carNames.Add("AI " + aiCount);
                ++aiCount;
            }
            else
                carNames.Add(currentPlayerScore.name);

    }

    private void DisplayScores()
    {
        for (uint i = 0; i < this.highScoresList.Count; ++i)
        {
            Debug.Log(this.highScoresList[(int)i].name + " (DISPLAY SCORES) " + this.highScoresList[(int)i].GetScoreAsString());
        }
    }

    public void ReloadScores(string filename)
    {
        this.fileName = filename + "HighScores.xml";
        LoadScores();
    }

    public void DeleteCurrentScores()
    {
        HighScoreSerializer serializer = new HighScoreSerializer(this.fileName);
        serializer.DeleteFile();
        this.highScoresList = null;
    }
}
