﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(BezierSpline))]
public class BezierSplineInspector : Editor
{
    private BezierSpline spline = null;
    private Transform handleTransform = null;
    private Quaternion handleRotation = Quaternion.identity;

    private const float directionScale = 0.5f;
    private const int stepsPerCurve = 10;

    private const float rotationScale = 5.0f;

    private const float handleSize = 0.04f;
    private const float pickSize = 0.06f;

    private int selectedIndex = -1;

    private static Color[] modeColors = {
                                            Color.white,
                                            Color.yellow,
                                            Color.cyan
                                        };

    private void OnSceneGUI()
    {
        spline = target as BezierSpline;

        handleTransform = spline.transform;
        handleRotation = Tools.pivotRotation == PivotRotation.Local ? handleTransform.rotation : Quaternion.identity;

        Vector3 p0 = ShowPoint(0);
        for (int i = 1; i < spline.ControlPointCount; i += 3)
        {
            Vector3 p1 = ShowPoint(i);
            Vector3 p2 = ShowPoint(i + 1);
            Vector3 p3 = ShowPoint(i + 2);

            Handles.color = Color.gray;
            Handles.DrawLine(p0, p1);
            Handles.DrawLine(p2, p3);

            Handles.DrawBezier(p0, p3, p1, p2, Color.white, null, 2.0f);

            p0 = p3;
        }

        ShowDirections();
        ShowRotations();
    }

    private void ShowDirections()
    {
        Handles.color = Color.green;
        Vector3 point = spline.GetPoint(0f);
        Handles.DrawLine(point, point + spline.GetDirection(0.0f) * directionScale);
        int steps = stepsPerCurve * spline.CurveCount;
        for (int i = 1; i <= steps; i++)
        {
            point = spline.GetPoint((float)i / (float)steps);
            Handles.DrawLine(point, point + spline.GetDirection((float)i / (float)steps) * directionScale);
        }
    }

    private void ShowRotations()
    {
        int steps = 1 + spline.CurveCount;
        Vector3 point = spline.transform.TransformPoint(spline.GetControlPoint(0));
        Vector3 dir = spline.transform.TransformPoint(spline.GetControlPoint(1)) - point;
        Vector3 rotDir = Vector3.Cross(dir.normalized,
            Quaternion.AngleAxis(spline.GetRotationAtControlPoint(0) * Mathf.Rad2Deg, dir) * Vector3.up);

        Vector3 upVector = Vector3.Cross(rotDir, dir.normalized);

        Handles.color = Color.cyan;
        Handles.DrawLine(point - (rotDir * (rotationScale / 2.0f)), point + (rotDir * (rotationScale / 2.0f)));

        Handles.color = Color.magenta;
        Handles.DrawLine(point + (upVector * (rotationScale / 3.0f)), point);

        for (int i = 0; i < steps; i++)
        {
            point = spline.transform.TransformPoint(spline.GetControlPoint(i * 3));
            dir = spline.transform.TransformPoint(spline.GetControlPoint(i == 0 ? 1 : (i * 3) - 1)) - point;
            rotDir = Vector3.Cross(dir.normalized,
            Quaternion.AngleAxis(spline.GetRotationAtControlPoint(i * 3) * Mathf.Rad2Deg, dir) * Vector3.up);
            upVector = Vector3.Cross(rotDir, dir.normalized);

            Handles.color = Color.cyan;
            Handles.DrawLine(point - (rotDir * (rotationScale / 2.0f)), point + (rotDir * (rotationScale / 2.0f)));

            Handles.color = Color.magenta;
            Handles.DrawLine(point + (upVector * (rotationScale / 3.0f)), point);
        }
    }

    private Vector3 ShowPoint(int index)
    {
        Vector3 point = handleTransform.TransformPoint(spline.GetControlPoint(index));
        float size = HandleUtility.GetHandleSize(point);
        if (index == 0)
            size *= 2.0f;

        Handles.color = modeColors[(int)spline.GetControlPointMode(index)];
        if (Handles.Button(point, handleRotation, size * handleSize, size * pickSize, Handles.DotCap))
        {
            selectedIndex = index;
            Repaint();
        }

        if (index == selectedIndex)
        {
            EditorGUI.BeginChangeCheck();
            point = Handles.DoPositionHandle(point, handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Move Point");
                EditorUtility.SetDirty(spline);
                spline.SetControlPoint(index, handleTransform.InverseTransformPoint(point));
            }
        }

        return point;
    }

    public override void OnInspectorGUI()
    {
        spline = target as BezierSpline;

        EditorGUI.BeginChangeCheck();
        bool loop = EditorGUILayout.Toggle("Loop", spline.Loop);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Toogle Loop");
            EditorUtility.SetDirty(spline);
            spline.Loop = loop;
        }

        if (selectedIndex >= 0 && selectedIndex < spline.ControlPointCount)
            DrawSelectedPointInspector();

        if (GUILayout.Button("Add Curve"))
        {
            Undo.RecordObject(spline, "Add Curve");
            spline.AddCurve();
            EditorUtility.SetDirty(spline);
        }
    }

    private void DrawSelectedPointInspector()
    {
        GUILayout.Label("Selected Point");
        EditorGUI.BeginChangeCheck();
        Vector3 point = EditorGUILayout.Vector3Field("Position", spline.GetControlPoint(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Move Point");
            EditorUtility.SetDirty(spline);
            spline.SetControlPoint(selectedIndex, point);
        }

        if (spline.IsWaypoint(selectedIndex))
        {
            EditorGUI.BeginChangeCheck();
            float rotation = EditorGUILayout.FloatField("Rotation", spline.GetRotationAtControlPoint(selectedIndex) * Mathf.Rad2Deg);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(spline, "Change Point Rotation");
                EditorUtility.SetDirty(spline);
                spline.SetRotationAtControlPoint(selectedIndex, Mathf.Deg2Rad * rotation);
            }
        }

        EditorGUI.BeginChangeCheck();
        BezierSpline.BezierControlPointMode mode = (BezierSpline.BezierControlPointMode)EditorGUILayout.EnumPopup("Mode", spline.GetControlPointMode(selectedIndex));
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(spline, "Change Point Mode");
            spline.SetControlPointMode(selectedIndex, mode);
            EditorUtility.SetDirty(spline);
        }
    }
}
